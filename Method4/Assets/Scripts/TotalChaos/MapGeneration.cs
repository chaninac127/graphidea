﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapGeneration : MonoBehaviour
{
    public int seed;

    public GameObject tileUnit;
    public bool randomSeed;
    public int roomsToPlace;

    public Vector2 mapSize;

    public Vector2 roomSizeX;
    public Vector2 roomSizeY;

    int mapX;
    int mapY;

    Node[,] grid;

    List<List<Node>> placedRoom = new List<List<Node>>(); // so we dont have to loop back later to find a room
    //public int[,] representationMap;
    //public List<(int,int)[]> rooms;

    private void Start()
    {
        mapX = Mathf.RoundToInt(mapSize.x);
        mapY = Mathf.RoundToInt(mapSize.y);
        //rooms = new List<(int, int)[]>();
        if (randomSeed)
        {
            seed = Random.Range(0, int.MaxValue);
        }
        Random.InitState(seed);
        GenerateMap();
    }

    bool checkBound(int posX, int posY, int sizeX, int sizeY)
    {
        if(posX + sizeX > mapX | posY + sizeY > mapY)
        {
            return true;
        }
        return false;
    }

    public void GenerateMap()
    {

        grid = new Node[mapX, mapY];

        for(int x = 0; x < mapX; x++)
        {
            for(int y = 0; y < mapY; y++)
            {
                grid[x, y] = new Node(x, y, 0); //default unwalkable
            }
        }

        for (int i = 2; i < roomsToPlace + 2; i++) //reserving 1 for walkway
        {
            List<Node> room = new List<Node>();

            int randomX = Random.Range(0, mapX);
            int randomY = Random.Range(0, mapY);

            int randomRoomSizeX = Mathf.RoundToInt(Random.Range(roomSizeX.x, roomSizeX.y));
            int randomRoomSizeY = Mathf.RoundToInt(Random.Range(roomSizeY.x, roomSizeY.y));

            bool overlap = false;

            for (int tries = 0; tries < 10; tries++)
            {
                if(checkBound(randomX, randomY, randomRoomSizeX, randomRoomSizeY))
                {
                    Debug.Log("Failed placing room attempt : " + tries);
                    randomX = Random.Range(0, mapX);
                    randomY = Random.Range(0, mapY);

                    randomRoomSizeX = Mathf.RoundToInt(Random.Range(roomSizeX.x, roomSizeX.y));
                    randomRoomSizeY = Mathf.RoundToInt(Random.Range(roomSizeY.x, roomSizeY.y));
                }
            }

            if (checkBound(randomX, randomY, randomRoomSizeX, randomRoomSizeY)){ // enough tries
                break;
            }


            for(int x = randomX; x < randomX + randomRoomSizeX; x++)
            {
                for(int y = randomY; y < randomY + randomRoomSizeY; y++)
                {
                    if(grid[x,y].state == 2) // check if overlap a room
                    {
                        overlap = true;
                        break;
                    }
                    grid[x, y].state = 2;
                    room.Add(grid[x, y]);
                }
                if (overlap)
                {
                    break;
                }
            }

            if (overlap)
            {
                foreach(Node node in room)
                {
                    node.state = 0; // reset;
                }
                Debug.LogError("reset a room due to overlap");
                break;
            }

            //placedRoom.Add(room); not yet we should add in later

            if(placedRoom.Count > 0) // more than 1 room now should connect the room somehow
            {
                List<Node> randomRoomToMatch = placedRoom[Random.Range(0, placedRoom.Count)];

                Node pointFromCurrentRoom = room[Random.Range(0, room.Count)];
                Node pointFromTarget = randomRoomToMatch[Random.Range(0, randomRoomToMatch.Count)];

                // now find path between these two points;
                List<Node> openSet = new List<Node>();
                HashSet<Node> closedSet = new HashSet<Node>();
                openSet.Add(pointFromCurrentRoom);

                while(openSet.Count > 0)
                {
                    Node node = openSet[0];
                    for(int j = 1; j < openSet.Count; j++)
                    {
                        if(openSet[j].fCost <  node.fCost || openSet[j].fCost == node.fCost)
                        {
                            if(openSet[j].hCost < node.hCost)
                            {
                                node = openSet[j];
                            }
                        }
                    }

                    openSet.Remove(node);
                    closedSet.Add(node);

                    foreach(Node neighbor in GetNeighbours(node))
                    {
                        if (closedSet.Contains(neighbor))
                        {
                            continue;
                        }

                        int newCostToNeighbour = node.gCost + GetDistance(node, neighbor);
                        if(newCostToNeighbour < neighbor.gCost || !openSet.Contains(neighbor))
                        {
                            neighbor.gCost = newCostToNeighbour;
                            neighbor.hCost = GetDistance(neighbor, pointFromTarget);
                            neighbor.parent = node;

                            if (!openSet.Contains(neighbor))
                            {
                                openSet.Add(neighbor);
                            }
                        }
                    }
                }

                List<Node> path = RetracePath(pointFromCurrentRoom, pointFromTarget);
                foreach(Node node in path)
                {
                    if(node.state == 0)
                    {
                        node.state = 1;
                    }
                }
            }

            placedRoom.Add(room);

        }

        for(int x = 0; x < mapX; x++)
        {
            //string row = "";
            for(int y = 0; y < mapY; y++)
            {
                Node currentNode = grid[x, y];
                //row += " " + grid[x,y].state;
                if(currentNode.state != 0)
                {
                    GameObject tile = Instantiate(tileUnit, new Vector3(x, 0, y), Quaternion.identity);
                    //tile.name = ("Tile coordinate (" + x + "," + y + ")");
                    tile.transform.parent = gameObject.transform;
                }
            }
            //Debug.Log(row);
        }

        Debug.Log("room placed : " + placedRoom.Count);


        //representationMap = new int[mapY, mapX];
        //for(int i = 2; i<roomsToPlace+2; i++)  // this loop is for test generation for now
        //{

        //    int randomX = Random.Range(0, mapX);
        //    int randomY = Random.Range(0, mapY);

        //    if (createRoom(randomX, randomY, i))
        //    {
        //        Debug.Log("Room placed");
        //    }
        //    else
        //    {
        //        Debug.LogError("Room placing error");
        //    }
        //}

        //Debug.Log("Total Rooms : " + rooms.Count);

        //foreach((int,int)[] room in rooms)
        //{
        //    Debug.Log(room.Length); // this should be the room size
        //}

        //for(int j = 0; j < mapY; j++) 
        //{
        //    for(int i = 0; i < mapX; i++)
        //    {
        //        if(representationMap[j,i] != 0)
        //        {
        //           GameObject tile = Instantiate(tileUnit, new Vector3(i, 0, j),Quaternion.identity);
        //           tile.transform.parent = gameObject.transform;
        //        }
        //    }
        //}

        //Debug.Log("Generation done");

    }

    List<Node> RetracePath(Node startNode, Node endNode)
    {
        List<Node> path = new List<Node>();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
        path.Reverse();

        return path;

    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

        if (dstX > dstY)
            return 14 * dstY + 10 * (dstX - dstY);
        return 14 * dstX + 10 * (dstY - dstX);
    }



    //public bool createRoom(int x, int y, int roomNumber) // need to make sure the number for room is not the same as the passageway...
    //{
    //    int randomedRoomSizeX = (int) Random.Range(roomSizeX.x, roomSizeX.y);
    //    int randomedRoomSizeY = (int) Random.Range(roomSizeY.x, roomSizeY.y);

    //    if( (x + randomedRoomSizeX >= representationMap.GetLength(1)) | (y + randomedRoomSizeY >= representationMap.GetLength(0))){ // out of bound
    //        return false;
    //    }
    //    else
    //    {
    //        (int, int)[] room = new (int, int)[randomedRoomSizeX * randomedRoomSizeY]; 
    //        List<(int, int)> coordinate = new List<(int, int)>(); // add the coordinate here to paint later
    //        for(int j = y; j < y + randomedRoomSizeY; j++)
    //        {
    //            for(int i = x; i< x + randomedRoomSizeX; i++)
    //            {
    //                if (representationMap[j, i] == 0)
    //                { //check if already is a part of another room or not;
    //                    // if not, add the coordinate to the list
    //                    coordinate.Add((j, i));
    //                }
    //                else
    //                {
    //                    // overlap another room, just return
    //                    return false;
    //                }
    //            }
    //        }
    //        // if got here that's mean the coordinates are clear to be make into a new room
    //        for(int i = 0; i < coordinate.Count; i++)
    //        {
    //            representationMap[coordinate[i].Item1, coordinate[i].Item2] = roomNumber; //set the tile to the room number;
    //            room[i] = (coordinate[i].Item1, coordinate[i].Item2);
    //        }

    //        rooms.Add(room);
    //    }

    //    return true;
    //}

    public List<Node> GetNeighbours(Node node)
    {
        List<Node> neighbours = new List<Node>();

        for (int x = -1; x <= 1; x++)
        {
            for (int y = -1; y <= 1; y++)
            {
                if (x == y || x == -y)
                    continue;

                int checkX = node.gridX + x;
                int checkY = node.gridY + y;

                if (checkX >= 0 && checkX < mapSize.x && checkY >= 0 && checkY < mapSize.y)
                {
                    neighbours.Add(grid[checkX, checkY]);
                }
            }
        }
        return neighbours;
    }

    public class Node
    {
        public int gridX;
        public int gridY;
        public int state;

        public int hCost;
        public int gCost;
        public Node parent;

        public Node(int _x, int _y, int _state)
        {
            gridX = _x;
            gridY = _y;
            state = _state;
        }

        public int fCost
        {
            get
            {
                return gCost + hCost;
            }
        }
    }
}
