﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Node
{
    public int minimumSpace = 15;

    public int minRoomWidth = 5;
    public int minRoomHeight = 5;

    public List<Rect> corridors = new List<Rect>();

    public int x, y, width, height;

    public int level;

    public Room roomInNode;

    public Node leftChild;
    public Node rightChild;
    public Node parent;

    public int cut; // 1 for horizontal, 2 for vertical
    public int cutIndex;

    public Color nodeColor;

    public Node(Node _parent)
    {
        parent = _parent;
    }

    public bool isLeaf()
    {
        return (leftChild == null && rightChild == null);
    }

    public void connectBetween(Node left, Node right)
    {
        Room leftRoom = leftChild.getRoom();
        Room rightRoom = rightChild.getRoom();

        Vector2 LeftCenter = new Vector2(leftRoom.xCenter, leftRoom.yCenter);
        Vector2 RightCenter = new Vector2(rightRoom.xCenter, rightRoom.yCenter);

        int wDif = (int)(RightCenter.x - LeftCenter.x);
        int hDif = (int)(RightCenter.y - LeftCenter.y);

        if(wDif < 0)
        {
            if(hDif < 0)
            {
                if(Random.Range(0f,1f) < 0.5f)
                {
                    corridors.Add(new Rect(RightCenter.x, LeftCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(RightCenter.x, RightCenter.y, 1, Mathf.Abs(hDif)));
                }
                else
                {
                    corridors.Add(new Rect(RightCenter.x, RightCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(LeftCenter.x, RightCenter.y, 1, Mathf.Abs(hDif)));
                }
            }else if (hDif > 0)
            {
                if (Random.Range(0f, 1f) < 0.5f)
                {
                    corridors.Add(new Rect(RightCenter.x, LeftCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(RightCenter.x, LeftCenter.y, 1, Mathf.Abs(hDif)));
                }
                else
                {
                    corridors.Add(new Rect(RightCenter.x, RightCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, 1, Mathf.Abs(hDif)));
                }

                corridors.Add(new Rect(RightCenter.x, RightCenter.y, Mathf.Abs(wDif), 1));
            }
        }
        else if(wDif > 0)
        {
            if(hDif < 0)
            {
                if(Random.Range(0f,1f) < 0.5f)
                {
                    corridors.Add(new Rect(LeftCenter.x, RightCenter.y, Mathf.Abs(wDif),1));
                    corridors.Add(new Rect(LeftCenter.x, RightCenter.y, 1, Mathf.Abs(hDif)));
                }
                else
                {
                    corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(RightCenter.x, RightCenter.y, 1, Mathf.Abs(hDif)));
                }
            }else if(hDif > 0)
            {
                if(Random.Range(0f,1f) < 0.5f)
                {
                    corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, Mathf.Abs(wDif), 1));
                    corridors.Add(new Rect(RightCenter.x, LeftCenter.y, 1, Mathf.Abs(hDif)));
                }
                else
                {
                    corridors.Add(new Rect(LeftCenter.x, RightCenter.y, Mathf.Abs(wDif),1));
                    corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, 1, Mathf.Abs(hDif)));
                }
            }
            else
            {
                corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, Mathf.Abs(wDif),1));
            }
        }
        else
        {
            if(hDif < 0)
            {
                corridors.Add(new Rect(RightCenter.x, RightCenter.y, 1, Mathf.Abs(hDif)));
            }else if (hDif > 0)
            {
                corridors.Add(new Rect(LeftCenter.x, LeftCenter.y, 1, Mathf.Abs(hDif)));
            }
        }
    }

    public Node(Node _parent, int _level, int _x, int _y, int _width, int _height)
    {
        parent = _parent;
        level = _level;
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        nodeColor = new Color(Random.Range(0, 1f), Random.Range(0, 1f), Random.Range(0, 1f));
    }

    public Room getRoom()
    {
        if(roomInNode != null)
        {
            return roomInNode;
        }
        else
        {
            if(leftChild != null)
            {
                Room leftRoom = leftChild.getRoom();
                if(leftRoom != null)
                {
                    return leftRoom;
                }
            }
            if(rightChild != null)
            {
                Room rightRoom = rightChild.getRoom();
                if(rightRoom != null)
                {
                    return rightRoom;
                }
            }
        }

        return null;
    }

    public bool initSplit()
    {
        if(!(isLeaf())) // already have child, cannot split again
        {
            return false;
        }

        bool splitH;
        if (width/height >= 1.25)
        {
            splitH = false;
        }
        else if(height/width >= 1.25)
        {
            splitH = true;
        }
        else
        {
            splitH = Random.Range(0f, 1f) > 0.5f;
        }

        if (!splitH)
        {
            int max = width - minimumSpace;
            if(max <= minimumSpace)
            {
                //too small
                return false;
            }

            cut = 1;

            int splittingIndex = Random.Range(x + minimumSpace, (int)(x + width) - minimumSpace);
            int partitionA = splittingIndex - x;
            int partitionB = ((x + width) - splittingIndex - 1);

            if (partitionA < minimumSpace || partitionB < minimumSpace)
            {
                //this one is a leaf
                return false;
            }

            leftChild = new Node(this, level + 1, x, y, partitionA, height);
            rightChild = new Node(this, level + 1, splittingIndex + 1, y, partitionB, height);
            cutIndex = splittingIndex;
            return true;
        }
        else
        {
            int max = height - minimumSpace;
            if (max <= minimumSpace)
            {
                //too small
                return false;
            }

            cut = 2;

            int splittingIndex = Random.Range(y + minimumSpace, (int)(y + height) - minimumSpace);
            int partitionA = splittingIndex - y;
            int partitionB = ((y + height) - splittingIndex - 1);

            if (partitionA < minimumSpace || partitionB < minimumSpace)
            {
                //this one is a leaf
                return false;
            }

            leftChild = new Node(this, level + 1, x, y, width, partitionA);
            rightChild = new Node(this, level + 1, x, splittingIndex + 1, width, partitionB);
            cutIndex = splittingIndex;
            return true;
        }
    }

    public void createRoom()
    {
        if(leftChild != null)
        {
            leftChild.createRoom();
        }
        if(rightChild != null)
        {
            rightChild.createRoom();
        }
        if(!isLeaf())
        {
            connectBetween(leftChild, rightChild);
        }
        else
        {
            int roomWidth = Random.Range(minRoomWidth, width);
            int roomHeight = Random.Range(minRoomHeight, height);

            int placementLimitX = (x + width) - roomWidth;
            int placementLimitY = (y + height) - roomHeight;

            int posX = Random.Range(x, placementLimitX);
            int posY = Random.Range(y, placementLimitY);

            roomInNode = new Room(posX, posY, roomWidth, roomHeight);
        }

        //if (width < minRoomWidth || height < minRoomHeight)
        //{
        //    return false;
        //}
    }



    //public bool splitHorizontal()
    //{
    //    cut = 1;
    //    int splittingIndex = Random.Range(y + Mathf.RoundToInt((height * 0.25f)), height - Mathf.RoundToInt((height * 0.25f)));
    //    int partitionA = splittingIndex - y;
    //    int partitionB = height - splittingIndex;

    //    if(partitionA < minimumSpace || partitionB < minimumSpace)
    //    {
    //        return false;
    //    }

    //    leftChild = new Node(this, level + 1, x, y, width, Mathf.RoundToInt(splittingIndex - y));
    //    rightChild = new Node(this, level + 1, x, Mathf.RoundToInt(splittingIndex + 1), width, Mathf.RoundToInt(height - splittingIndex));

    //    cutIndex = splittingIndex;

    //    return true;
    //}

    //public bool splitVerticle()
    //{
    //    cut = 2;
    //    int splittingIndex = Random.Range(x + (int)(width * 0.25f), width - (int)(width * 0.25f));
    //    int partitionA = splittingIndex - x;
    //    int partitionB = width - splittingIndex;

    //    if (partitionA < minimumSpace || partitionB < minimumSpace)
    //    {
    //        return false;
    //    }

    //    leftChild = new Node(this, level + 1, x, y, splittingIndex - x, height);
    //    rightChild = new Node(this, level + 1, splittingIndex + 1, y, width - splittingIndex, height);

    //    cutIndex = splittingIndex;

    //    return true;
    //}
}
