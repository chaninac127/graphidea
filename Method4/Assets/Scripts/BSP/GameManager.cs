﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour
{
    public int maxlevel;

    public Vector2 gridSize;

    public int seed;

    public int minRoomWidth = 5;
    public int minRoomHeight = 5;

    [Range(0, 1)]
    public float roomPercentage;

    public GameObject tile;
    public GameObject obstacle;

    public List<Node> viableNodes = new List<Node>();

    public Queue<Node> splitQueue = new Queue<Node>();

    //int[,] checkingGrid;

    gridCell[,] gridCellGrid;

    private void Start()
    {
        //checkingGrid = new int[(int)gridSize.x, (int)gridSize.y];
        gridCellGrid = new gridCell[(int)gridSize.x, (int)gridSize.y];
        for (int i = 0;i < gridSize.x; i++)
        {
            for(int j = 0;j < gridSize.y; j++)
            {
                //checkingGrid[i, j] = 0;
                gridCellGrid[i, j] = new gridCell(i, j, 0);
            }
        }

        GenerateBSP();
    }

    public void GenerateBSP()
    {
        Node root = new Node(null, 0, 0, 0, (int)gridSize.x, (int)gridSize.y);
        splitQueue.Enqueue(root);


        while (splitQueue.Count > 0)
        {
            Node toSplit = splitQueue.Dequeue();

            if (toSplit.initSplit())
            {
                splitQueue.Enqueue(toSplit.leftChild);
                splitQueue.Enqueue(toSplit.rightChild);
            }
            else
            {
                Debug.LogError("Cannot be split : probably too small");
            }
        }

        Debug.Log("Done BSPing");
        Debug.Log("number of leaves : " + viableNodes.Count);
        Debug.Log("Connecting Rooms");
        root.createRoom();
        DrawRooms(root);
        Debug.Log("Drawing Corridors");
        DrawCorridorsOnGrid(root);

        for(int i = 0; i < gridSize.x; i++)
        {
            for(int j = 0; j < gridSize.y; j++)
            {
                Color obstacleColor = new Color(1, 0, 0);
                if(gridCellGrid[i,j].state == 0)
                {
                    GameObject toSpawn = Instantiate(obstacle, new Vector3(i, 0, j), Quaternion.identity);
                    var toColor = toSpawn.GetComponentsInChildren<Renderer>();
                    foreach(Renderer render in toColor)
                    {
                        render.material.color = obstacleColor;
                    }
                    toSpawn.name = "tile (" + i + "," + j + ")";
                    toSpawn.transform.parent = gameObject.transform;

                }
                else
                {
                    GameObject toSpawn = Instantiate(tile, new Vector3(i, 0, j), Quaternion.identity);
                    Instantiate(toSpawn, new Vector3(i, 0, j), Quaternion.identity);
                    toSpawn.name = "tile (" + i + "," + j + ")";
                    toSpawn.transform.parent = gameObject.transform;
                }

            }
        }

        //for checking the grid
        //for (int i = 0; i < gridSize.x; i++)
        //    {
        //        string line = "";
        //        for (int j = 0; j < gridSize.y; j++)
        //        {
        //            line += gridCellGrid[i, j].state + " ";
        //        }
        //        Debug.Log(line);
        //}


        //int roomsNumber = (int)((viableNodes.Count) * roomPercentage);
        //Debug.Log("number of room in percentage : " + roomPercentage + "% is " + roomsNumber);

        //shuffle the viableNodes and get only numbers of rooms
        //for (int i = 0; i < roomsNumber; i++)
        //{
        //    int randomNodeIndex = Random.Range(0, viableNodes.Count);
        //    Node randomedNode = viableNodes[randomNodeIndex];

        //    randomedNode.createRoom();
        //    Room nodeRoom = randomedNode.roomInNode;
        //    GameObject tilesHolder = Instantiate(new GameObject("room " + i), Vector3.zero, Quaternion.identity);
        //    tilesHolder.transform.parent = gameObject.transform;
        //    for(int x = nodeRoom.x; x < (nodeRoom.x + nodeRoom.width); x++)
        //    {
        //        for(int y = nodeRoom.y; y < (nodeRoom.y + nodeRoom.height); y++)
        //        {
        //            GameObject tileUnit = Instantiate(tile, new Vector3(x, 0, y), Quaternion.identity);
        //            tileUnit.transform.name = "tile (" + x + "," + y + ")";
        //            tileUnit.transform.parent = tilesHolder.transform;
        //            checkingGrid[x, y] = 1;
        //        }
        //    }

        //    //these are for checking
        //    //Vector3 spawnPosition = new Vector3(randomedNode.x + (randomedNode.width * 0.5f), 0, randomedNode.y + (randomedNode.height * 0.5f));
        //    //GameObject area = Instantiate(tile, spawnPosition, Quaternion.identity);
        //    //area.transform.localScale = new Vector3(randomedNode.width, 1f, randomedNode.height);
        //    //var toColor = area.GetComponentsInChildren<Renderer>();
        //    //foreach(Renderer render in toColor)
        //    //{
        //    //    render.material.color = randomedNode.nodeColor;
        //    //}



        //    nodesWithRooms.Add(randomedNode);
        //    viableNodes.RemoveAt(randomNodeIndex);
        //}
    }


    //public void spawnCells(Node node) // this are just the sections, do remember that
    //{
    //    if(node.leftChild == null && node.rightChild == null) //these are leaves
    //    {
    //        viableNodes.Add(node);
    //        //for checking
    //        //Vector3 spawnPosition = new Vector3(node.x + (node.width * 0.5f), 0, node.y + (node.height * 0.5f));
    //        //GameObject area = Instantiate(tile, spawnPosition, Quaternion.identity);
    //        //area.transform.localScale = new Vector3(node.width, 1f, node.height);
    //        //var toColor = area.GetComponentsInChildren<Renderer>();
    //        //foreach (Renderer render in toColor)
    //        //{
    //        //    render.material.color = node.nodeColor;
    //        //}
    //    }
    //    else
    //    {
    //        spawnCells(node.leftChild);
    //        spawnCells(node.rightChild);
    //    }
    //}

    void DrawRooms(Node node)
    {
        if(node == null)
        {
            return;
        }

        if (node.roomInNode != null)
        {
            for(int i = (int)node.roomInNode.x; i < (node.roomInNode.x + node.roomInNode.width); i++)
            {
                for(int j = (int)node.roomInNode.y; j < (node.roomInNode.y + node.roomInNode.height); j++)
                {
                    //if (i == (int)node.roomInNode.x || i == (node.roomInNode.x + node.roomInNode.width - 1) || j == (int)node.roomInNode.y || j == (node.roomInNode.y + node.roomInNode.height - 1))
                    //{
                    //    gridCellGrid[i, j].state = 2; //walls
                    //}
                    //else
                    //{
                        gridCellGrid[i, j].state = 1; //roomfloor
                    //}
                }
            }
        }
        else
        {
            DrawRooms(node.leftChild);
            DrawRooms(node.rightChild);
        }
    }

    void DrawCorridorsOnGrid(Node node)
    {
        if(node == null)
        {
            return;
        }

        DrawCorridorsOnGrid(node.leftChild);
        DrawCorridorsOnGrid(node.rightChild);
        if(node.corridors.Count == 1)
        {
            Debug.Log("only 1 corridor");
        }
        foreach (Rect corridor in node.corridors)
        {

            //Debug.Log("corridorX : " + corridor.x);
            //Debug.Log("corridorxMax : " + corridor.xMax);
            //Debug.Log("corridorY : " + corridor.y);
            //Debug.Log("corridoryMax : " + corridor.yMax);

            for (int i = (int)corridor.x; i < corridor.xMax; i++)
            {
                for(int j = (int)corridor.y; j < corridor.yMax; j++)
                {
                    //Debug.Log("(i,j) = " + i + ", " + j);
                    if(gridCellGrid[i, j].state == 0)
                    {
                        gridCellGrid[i, j].state = 3;
                    }
                }
            }
        }
    }

    public class gridCell
    {
        public int gridX;
        public int gridY;
        public int state;

        public int hCost;
        public int gCost;
        public gridCell parent;

        public gridCell(int _x, int _y, int _state)
        {
            gridX = _x;
            gridY = _y;
            state = _state;
        }

        public int fCost
        {
            get
            {
                return gCost + hCost;
            }
        }
    }

    //public List<gridCell> GetNeighbours(gridCell node)
    //{
    //    List<gridCell> neighbours = new List<gridCell>();

    //    for (int x = -1; x <= 1; x++)
    //    {
    //        for (int y = -1; y <= 1; y++)
    //        {
    //            if (x == y || x == -y)
    //                continue;

    //            int checkX = node.gridX + x;
    //            int checkY = node.gridY + y;

    //            if (checkX >= 0 && checkX < gridSize.x && checkY >= 0 && checkY < gridSize.y)
    //            {
    //                neighbours.Add(gridCellGrid[checkX, checkY]);
    //            }
    //        }
    //    }
    //    return neighbours;
    //}

    //List<gridCell> RetracePath(gridCell startNode, gridCell endNode)
    //{
    //    List<gridCell> path = new List<gridCell>();
    //    gridCell currentNode = endNode;

    //    while (currentNode != startNode)
    //    {
    //        path.Add(currentNode);
    //        currentNode = currentNode.parent;
    //    }
    //    path.Reverse();

    //    return path;

    //}

    //int GetDistance(gridCell nodeA, gridCell nodeB)
    //{
    //    int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
    //    int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);

    //    if (dstX > dstY)
    //        return 14 * dstY + 10 * (dstX - dstY);
    //    return 14 * dstX + 10 * (dstY - dstX);
    //}
}
  