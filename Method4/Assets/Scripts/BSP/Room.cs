﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room
{
    public int x, y, width, height;

    public int xCenter, yCenter;

    public Room(int _x, int _y, int _width, int _height)
    {
        x = _x;
        y = _y;
        width = _width;
        height = _height;
        xCenter = (x + Mathf.RoundToInt(_width / 2.0f));
        yCenter = (y + Mathf.RoundToInt(_height / 2.0f));
    }
}
